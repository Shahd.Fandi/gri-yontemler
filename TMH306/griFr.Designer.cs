﻿
namespace TMH306
{
    partial class griFr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.griYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ortalamaGriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lumaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aciklikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renkKanalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeEdilmisRenkKanaliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.islemBox = new System.Windows.Forms.PictureBox();
            this.kaynakBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.griYöntemleriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1625, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(109, 26);
            this.açToolStripMenuItem.Text = "Aç";
            // 
            // griYöntemleriToolStripMenuItem
            // 
            this.griYöntemleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ortalamaGriToolStripMenuItem,
            this.bT709ToolStripMenuItem,
            this.lumaToolStripMenuItem,
            this.aciklikToolStripMenuItem,
            this.renkKanalToolStripMenuItem,
            this.normalizeEdilmisRenkKanaliToolStripMenuItem});
            this.griYöntemleriToolStripMenuItem.Name = "griYöntemleriToolStripMenuItem";
            this.griYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(117, 24);
            this.griYöntemleriToolStripMenuItem.Text = "Gri Yöntemleri";
            // 
            // ortalamaGriToolStripMenuItem
            // 
            this.ortalamaGriToolStripMenuItem.Name = "ortalamaGriToolStripMenuItem";
            this.ortalamaGriToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.ortalamaGriToolStripMenuItem.Text = "Ortalama gri";
            this.ortalamaGriToolStripMenuItem.Click += new System.EventHandler(this.ortalamaGriToolStripMenuItem_Click);
            // 
            // bT709ToolStripMenuItem
            // 
            this.bT709ToolStripMenuItem.Name = "bT709ToolStripMenuItem";
            this.bT709ToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.bT709ToolStripMenuItem.Text = "BT-709";
            this.bT709ToolStripMenuItem.Click += new System.EventHandler(this.bT709ToolStripMenuItem_Click);
            // 
            // lumaToolStripMenuItem
            // 
            this.lumaToolStripMenuItem.Name = "lumaToolStripMenuItem";
            this.lumaToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.lumaToolStripMenuItem.Text = "Luma";
            this.lumaToolStripMenuItem.Click += new System.EventHandler(this.lumaToolStripMenuItem_Click);
            // 
            // aciklikToolStripMenuItem
            // 
            this.aciklikToolStripMenuItem.Name = "aciklikToolStripMenuItem";
            this.aciklikToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.aciklikToolStripMenuItem.Text = "Aciklik";
            this.aciklikToolStripMenuItem.Click += new System.EventHandler(this.aciklikToolStripMenuItem_Click);
            // 
            // renkKanalToolStripMenuItem
            // 
            this.renkKanalToolStripMenuItem.Name = "renkKanalToolStripMenuItem";
            this.renkKanalToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.renkKanalToolStripMenuItem.Text = "Renk kanal";
            this.renkKanalToolStripMenuItem.Click += new System.EventHandler(this.renkKanalToolStripMenuItem_Click);
            // 
            // normalizeEdilmisRenkKanaliToolStripMenuItem
            // 
            this.normalizeEdilmisRenkKanaliToolStripMenuItem.Name = "normalizeEdilmisRenkKanaliToolStripMenuItem";
            this.normalizeEdilmisRenkKanaliToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.normalizeEdilmisRenkKanaliToolStripMenuItem.Text = "Normalize edilmis renk kanali";
            this.normalizeEdilmisRenkKanaliToolStripMenuItem.Click += new System.EventHandler(this.normalizeEdilmisRenkKanaliToolStripMenuItem_Click);
            // 
            // islemBox
            // 
            this.islemBox.Location = new System.Drawing.Point(973, 101);
            this.islemBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.islemBox.Name = "islemBox";
            this.islemBox.Size = new System.Drawing.Size(613, 591);
            this.islemBox.TabIndex = 7;
            this.islemBox.TabStop = false;
            // 
            // kaynakBox
            // 
            this.kaynakBox.Location = new System.Drawing.Point(37, 101);
            this.kaynakBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.kaynakBox.Name = "kaynakBox";
            this.kaynakBox.Size = new System.Drawing.Size(613, 591);
            this.kaynakBox.TabIndex = 6;
            this.kaynakBox.TabStop = false;
            // 
            // griFr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1625, 793);
            this.Controls.Add(this.islemBox);
            this.Controls.Add(this.kaynakBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "griFr";
            this.Text = "griFr";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem griYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ortalamaGriToolStripMenuItem;


        private System.Windows.Forms.PictureBox islemBox;
        private System.Windows.Forms.PictureBox kaynakBox;
        private System.Windows.Forms.ToolStripMenuItem bT709ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lumaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aciklikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renkKanalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeEdilmisRenkKanaliToolStripMenuItem;
    }
}