﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TMH306
{
    public partial class griFr : Form
    {
        Bitmap kaynak, islem;
        public griFr()
        {
            InitializeComponent();
        }

        private void ortalamaGriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int red = piksel.R;
                    int green = piksel.G;
                    int blue = piksel.B;

                    int griDeger = (red + green + blue) / 3;

                    Color yPiksel = Color.FromArgb(griDeger, griDeger, griDeger);
                    islem.SetPixel(x, y, yPiksel);
                }
            }

            islemBox.Image = islem;
        }

        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int red = piksel.R;
                    int green = piksel.G;
                    int blue = piksel.B;

                    double griDeger = (red * 0.2125 + green * 0.7154 + blue * 0.072);
                    int gri = Convert.ToInt32(griDeger);
                    Color yPiksel = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void aciklikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int[] dizi = { piksel.R, piksel.G, piksel.B };
                    int gri = (dizi.Min() + dizi.Max()) / 2;

                    Color yPiksel = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void renkKanalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    Color griPiksel = Color.FromArgb(piksel.R, piksel.R, piksel.R);

                    Color yPiksel = Color.FromArgb(piksel.R, piksel.R, piksel.R);
                    islem.SetPixel(x, y, yPiksel);



                }
            }
            islemBox.Image = islem;
        }

        private void normalizeEdilmisRenkKanaliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int T = piksel.R + piksel.G + piksel.B;
                    if (T > 0)
                    {
                        int Ir = (255 + piksel.R) / T;
                    }
                    Color gri = Color.FromArgb(piksel.R, piksel.R, piksel.R);
                    islem.SetPixel(x, y, gri);
                }
            }
            islemBox.Image = islem;
        }

            private void lumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int red = piksel.R;
                    int green = piksel.G;
                    int blue = piksel.B;

                    double griDeger = (red * 0.3 + green * 0.59 + blue * 0.11);
                    int gri = Convert.ToInt32(griDeger);
                    Color yPiksel = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yPiksel);
                }
            }
            islemBox.Image = islem;
        }
    }
}

       
